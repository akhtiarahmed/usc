package com.booking;

import java.util.ArrayList;
import java.util.Scanner;

public class TTL {

	Scanner input = new Scanner(System.in);

	public void ttLD() {

		ArrayList<String> arrayDay = new ArrayList<>();
		arrayDay.add("Saturday");
		arrayDay.add("Sunday");

		System.out.println("Please enter Saturday or Sunday");
		String weekName = input.next();
		if (!arrayDay.contains(weekName)) {
			System.out.println("Invalid day, Please try again");
		} else {
			if (weekName.equalsIgnoreCase("Saturday")) {
				String t[][] = { { "Saturday", "05-03-2022", "8.30am", "Yoga", "$8" },
						{ "Saturday", "05-03-2022", "4.30pm", "Aquacise", "$20" },
						{ "Saturday", "05-03-2022", "7pm", "Box Fit", "$18" },
						{ "Saturday", "12-03-2022", "8.30am", "Yoga", "$8" },
						{ "Saturday", "12-03-2022", "4.30pm", "Aquacise", "$20" },
						{ "Saturday", "12-03-2022", "7pm", "Box Fit", "$18" }

				};
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] t1 : t) {
					System.out.format("%5s %13s %9s %10s %9s", t1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");
			} else {
				String t[][] = { { "Sunday", "06-03-2022", "7.30am", "Yoga", "$8" },
						{ "Sunday", "06-03-2022", "4.30pm", "Body Blitz", "$15" },
						{ "Sunday", "06-03-2022", "7.30pm", "Zumba", "$16" },
						{ "Sunday", "13-03-2022", "7.30am", "Yoga", "$8" },
						{ "Sunday", "13-03-2022", "4.30pm", "Zumba", "$16" },
						{ "Sunday", "13-03-2022", "7.30pm", "Body Blitz", "$15" }

				};
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] t1 : t) {
					System.out.format("%5s %13s %9s %10s %9s", t1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			}
		}
	}

	public void ttExercise() {

		ArrayList<String> arrayExercise = new ArrayList<>();
		arrayExercise.add("Yoga");
		arrayExercise.add("Zumba");
		arrayExercise.add("Aquacise");
		arrayExercise.add("Box Fit");
		arrayExercise.add("Body Blitz");
		System.out.println("Please enter Select the following Exercise");
		System.out.println(
				"----------------------------------------------------------------------------------------------");
		System.out.println(":::: Yoga, Zumba, Aquacise, Box Fit, Body Blitz ::::");
		System.out.println(
				"----------------------------------------------------------------------------------------------");
		String exercise = input.next();
		if (!arrayExercise.contains(exercise)) {
			System.out.println("Invalid day, Please try again");
		} else {

			if (exercise.equalsIgnoreCase("Yoga")) {

				String d[][] = { { "Saturday", "05-03-2022", "8.30am", "Yoga", "$8" },
						{ "Sunday", "06-03-2022", "7.30am", "Yoga", "$8" },
						{ "Saturday", "12-03-2022", "8.30am", "Yoga", "$8" },
						{ "Sunday", "13-03-2022", "7.30am", "Yoga", "$8" } };
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] d1 : d) {
					System.out.format("%5s %13s %9s %10s %9s", d1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			} else if (exercise.equalsIgnoreCase("Zumba")) {

				String d[][] = { { "Sunday", "06-03-2022", "7.30pm", "Zumba", "$16" },
						{ "Sunday", "13-03-2022", "4.30pm", "Zumba", "$16" } };
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] d1 : d) {
					System.out.format("%5s %13s %9s %10s %9s", d1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			} else if (exercise.equalsIgnoreCase("Aquacise")) {

				String d[][] = { { "Saturday", "05-03-2022", "4.30pm", "Aquacise", "$20" },
						{ "Saturday", "12-03-2022", "4.30pm", "Aquacise", "$20" } };
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] d1 : d) {
					System.out.format("%5s %13s %9s %10s %9s", d1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			} else if (exercise.equalsIgnoreCase("Box Fit")) {

				String t[][] = { { "Saturday", "05-03-2022", "7pm", "Box Fit", "$18" },
						{ "Saturday", "12-03-2022", "7pm", "Box Fit", "$18" } };
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] t1 : t) {
					System.out.format("%5s %13s %9s %10s %9s", t1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			} else if (exercise.equalsIgnoreCase("Body Blitz")) {

				String t[][] = { { "Sunday", "06-03-2022", "4.30pm", "Body Blitz", "$15" },
						{ "Sunday", "13-03-2022", "7.30pm", "Body Blitz", "$15" } };
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %15s %10s %9s", "Week", "Date", "Timing", "Lesson", "Price");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (String[] t1 : t) {
					System.out.format("%5s %13s %9s %10s %9s", t1);
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");

			} else {
				System.out.println("Invalid Input....!!!");
			}

		}

	}

}
