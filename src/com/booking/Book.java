package com.booking;

public class Book {

	private int sId;
	private String sName;
	private String sAddress;
	private String sPhoneNumber;
	private String lesson;
	private String statusOfLesson;
	private int rating;
	private int price;

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getsAddress() {
		return sAddress;
	}

	public void setsAddress(String sAddress) {
		this.sAddress = sAddress;
	}

	public String getsPhoneNumber() {
		return sPhoneNumber;
	}

	public void setsPhoneNumber(String sPhoneNumber) {
		this.sPhoneNumber = sPhoneNumber;
	}

	public String getLesson() {
		return lesson;
	}

	public void setLesson(String lesson) {
		this.lesson = lesson;
	}

	public String getStatusOfLesson() {
		return statusOfLesson;
	}

	public void setStatusOfLesson(String statusOfLesson) {
		this.statusOfLesson = statusOfLesson;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Book(int sId, String sName, String sAddress, String sPhoneNumber, String lesson, String statusOfLesson,
			int rating, int price) {
		super();
		this.sId = sId;
		this.sName = sName;
		this.sAddress = sAddress;
		this.sPhoneNumber = sPhoneNumber;
		this.lesson = lesson;
		this.statusOfLesson = statusOfLesson;
		this.rating = rating;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Book [sId=" + sId + ", sName=" + sName + ", sAddress=" + sAddress + ", sPhoneNumber=" + sPhoneNumber
				+ ", lesson=" + lesson + ", statusOfLesson=" + statusOfLesson + ", rating=" + rating + ", price="
				+ price + "]";
	}

}
