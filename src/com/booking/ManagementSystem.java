package com.booking;

import java.util.Scanner;

public class ManagementSystem {

	public static void main(String[] args) {


		int sId;
		String sName;
		String sAddress;
		String sPhoneNumber;
		String lesson;
		String statusOfLesson;
		int rating;
		int price;
		String tt1;

		int arrayStudent = 0;
		char oc = 'Y';

		Book b[] = new Book[10];
		TTL tt = new TTL();

		while (oc == 'Y') {
			Scanner scanner = new Scanner(System.in);
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("           Welcome to The University Sports Centre (USC)");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("1: TT Chart of lesoon offered");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("2: Book Appointment");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("3: Change Booking");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("4: Rate Lesson");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("5: Print Report Displaying the Average Rating for group exercise");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("6: Print Report Displaying the highest income of group exercise");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("7: Exist Application");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("                  Please Feel Free to Choice the Service's");
			System.out.println("******************************************************************************");

			int choice = scanner.nextInt();


			switch (choice) {

			case 1:
				System.out.println("*****View TimeTable By Day/Excerise*****");
				tt1 = scanner.next();
				
				if (tt1.equalsIgnoreCase("Day")) {
					tt.ttLD();
				} else {
					tt.ttExercise();
				}
				break;

			case 2:
				System.out.println("*****Book Appointment*****");
				System.out.println("Student Id");
				sId = scanner.nextInt();
				System.out.println("Student Name");
				sName = scanner.next();
				System.out.println("Student address");
				sAddress = scanner.next();
				System.out.println("Student phone number");
				sPhoneNumber = scanner.next();
				System.out.println("Lesson booked");
				lesson = scanner.next();
				System.out.println("Booking status");
				statusOfLesson = scanner.next();
				System.out.println("Rating the exercise");
				rating = scanner.nextInt();
				System.out.println("Price of excerise");
				price = scanner.nextInt();

				b[arrayStudent] = new Book(sId, sName, sAddress, sPhoneNumber, lesson, statusOfLesson, rating, price);
				arrayStudent++;
				System.out.println("Appointment Booked Successfully..!!");
				break;

			case 3:
				System.out.println("*****Change Appointment Booking*****");
				System.out.println("Do you want to 'CANCEL' OR 'REBOOK' the Appointment");
				System.out.println("Student Id");
				sId = scanner.nextInt();
				for (int i = 0; i < arrayStudent; i++) {
					if (b[i] != null && b[i].getsId() == sId) {
						System.out.println("Booking status");
						statusOfLesson = scanner.next();
						if (statusOfLesson.equals("CANCEL"))
							System.out.println("Appointment is Cancelled Successfully..!!");
						else
							System.out.println("Lesson Booking");
						lesson = scanner.next();
						System.out.println("Appointment is Rescheduled Successfully..!!");
						b[i].setStatusOfLesson(statusOfLesson);
						b[i].setLesson(lesson);
					}
				}
				break;

			case 4:
				System.out.println("*****Rate Lesson*****");
				System.out.println("Student Id");
				sId = scanner.nextInt();
				for (int i = 0; i < arrayStudent; i++) {
					if (b[i] != null && b[i].getsId() == sId) {
						System.out.println(
								"Please rate lesson in ranging from 1 to 5 (1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied).");
						rating = scanner.nextInt();
						System.out.println("Thank you for rating the lesson..!!");
						b[i].setRating(rating);
					}
				}
				break;

			case 5:
				System.out.println("*****Report Displaying the Average Rating for group exercise*****");
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s %14s %15s %9s %10s", "Student Id", "Student Name", "Student Addr",
						"Phone Number", "Lesson", "Rating");
				System.out.println();
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				for (int i = 0; i < arrayStudent; i++) {

					System.out.format("%5s %12s %15s %16s %12s %9s", b[i].getsId(), b[i].getsName(),
							b[i].getsAddress(), b[i].getsPhoneNumber(), b[i].getLesson(), b[i].getRating());
					System.out.println();
				}
				System.out.println(
						"----------------------------------------------------------------------------------------------");
				break;

			case 6:
				System.out.println("*****Report Displaying the highest income of group exercise*****");
				System.out.println(
						"---------------------------------------------------------------------------------------------");
				System.out.printf("%5s %10s", "Lesson Name", "Income");
				System.out.println();
				int Yoga = 0, Zumba = 0, Aquacise = 0, BoxFit = 0, BodyBlitz = 0;
				for (int i = 0; i < arrayStudent; i++) {

					if (b[i].getLesson().equalsIgnoreCase("Yoga")) {
						for (int j = 0; j < arrayStudent; j++) {
							Yoga = Yoga + b[j].getPrice();
						}

					} else if (b[i].getLesson().equalsIgnoreCase("Zumba")) {
						for (int j = 0; j <arrayStudent; j++) {
							Zumba = Zumba + b[j].getPrice();
						}
					} else if (b[i].getLesson().equalsIgnoreCase("Aquacise")) {
						for (int j = 0; j <arrayStudent; j++) {
							Aquacise = Aquacise + b[j].getPrice();
						}
					} else if (b[i].getLesson().equalsIgnoreCase("BoxFit")) {
						for (int j = 0; j <arrayStudent; j++) {
							BoxFit = BoxFit + b[j].getPrice();
						}
					} else if (b[i].getLesson().equalsIgnoreCase("BodyBlitz")) {
						for (int j = 0; j < arrayStudent; j++) {
							BodyBlitz = BodyBlitz + b[j].getPrice();
						}
					} else {
						System.out.println("sortry");
					}
				}
				System.out.format("%5s %12s", "Yoga", Yoga + "$");
				System.out.println();
				System.out.format("%5s %12s", "Zumba", Zumba + "$");
				System.out.println();
				System.out.format("%5s %12s", "Aquacise", Aquacise + "$");
				System.out.println();
				System.out.format("%5s %12s", "BoxFit", BoxFit + "$");
				System.out.println();
				System.out.format("%5s %12s", "Body Blitz", BodyBlitz + "$");
				System.out.println();
				System.out.println(
						"----------------------------------------------------------------------------------------------");
				break;

			case 7:
				System.out.println("Thank you for using The University Sports Centre (USC) Application...!!!");
				break;

			default:
				System.out.println("Sorry...!! You enter a worng choice...!!");
				System.out.println("Thank You...!! Please try again...!!");
				break;

			}
			System.out.println("Do you like to continue...!!");
			oc = scanner.next().charAt(0);
			if (Character.isLowerCase(oc)) {
				oc = Character.toUpperCase(oc);
			}

		}
	}
}
